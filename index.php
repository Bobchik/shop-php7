<?php
/**
 * Created by PhpStorm.
 * User: Bobka
 * Date: 14.11.2017
 * Time: 0:37
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);

session_start();

//Подключение файлов системы
define('ROOT', dirname(__FILE__));

require_once (ROOT.'/components/Autoload.php');

$route = new Router();
$route->run();

?>
