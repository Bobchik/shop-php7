<?php
/**
 * Created by PhpStorm.
 * User: Bobka
 * Date: 14.11.2017
 * Time: 17:53
 */

class DB {
	public static function getConnection()
	{
		$paramsPath = ROOT.'/config/env.php';
		$params = include ($paramsPath);

		$db = new PDO("mysql:host={$params['host']};dbname={$params['dbname']}", $params['user'], $params['password']);

		return $db;
	}
}