<?php
/**
 * Created by PhpStorm.
 * User: Bobka
 * Date: 02.12.2017
 * Time: 0:14
 */

abstract class AdminBase {

	public static function checkAdmin()
	{
		$userId = User::checkLogged();

		$user = User::getUserById($userId);

		if ($user['role'] == 'admin'){
			return true;
		}

		die('404');
	}

}