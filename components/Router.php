<?php
/**
 * Created by PhpStorm.
 * User: Bobka
 * Date: 14.11.2017
 * Time: 1:03
 */
class Router{

	public $routes;

	public function __construct()
	{
		$routesPath = ROOT.'/config/routes.php';
		$this->routes = include ($routesPath);
	}

	private function getURI() {
		if (isset($_SERVER['REQUEST_URI'])) return trim($_SERVER['REQUEST_URI']);
	}

	public function run()
	{
		//Получаем строку запроса
		$uri = $this->getURI();

		//Проверяем наличие такой строки в роутах
		//Если есть совпадение, выбираем какой экшн и контроллер обрабатывают
		foreach ($this->routes as $uriPattern => $path){

			if (preg_match("~$uriPattern~", $uri)){

				//Получаем внутрений путь из внещнего
				$internalRoute = preg_replace("~$uriPattern~", $path, $uri);
				$segments = explode('/', $internalRoute);
				array_shift($segments);

				$controllerName = array_shift($segments).'Controller';
				$controllerName = ucfirst($controllerName);

				$actionName = 'action'.ucfirst(array_shift($segments));

				$parameters = $segments;
				//Подключаем контроллер

                $controllerFile = ROOT.'/controllers/'.$controllerName.'.php';

                if (file_exists($controllerFile)) include_once ($controllerFile);

                //Создаём экземпляр этого класса и вызываем метод
                $controllerObject = new $controllerName;

                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
//				$result = $controllerObject->$actionName($parameters);

				if ($result != null){
					break;
				}
			}
		}
	}
}