<?php
/**
 * Created by PhpStorm.
 * User: Bobka
 * Date: 01.12.2017
 * Time: 12:08
 */

class Order {

	public static function getOrders()
	{
		$db = DB::getConnection();

		$sql = 'SELECT * FROM orders ORDER BY id DESC';

		$result = $db->query($sql);

		return $result;
	}

	public static function getOrderById($id)
	{
		$db = DB::getConnection();

		$sql = "SELECT * FROM orders WHERE id =:id";

		$result = $db->prepare($sql);
		$result->bindParam(":id", $id, PDO::PARAM_INT);

		$result->setFetchMode(PDO::FETCH_ASSOC);

		$result->execute();

		return $result->fetch();
	}

	public static function updateOrderById($id, $userName, $userPhone, $userComment, $date, $status)
	{
		$db = DB::getConnection();

		$sql = "UPDATE orders SET
				user_name = :user_name, 
                user_phone = :user_phone, 
                user_comment = :user_comment, 
                date = :date, 
                status = :status 
            	WHERE id = :id";

		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->bindParam(':user_name', $userName, PDO::PARAM_STR);
		$result->bindParam(':user_phone', $userPhone, PDO::PARAM_STR);
		$result->bindParam(':user_comment', $userComment, PDO::PARAM_STR);
		$result->bindParam(':date', $date, PDO::PARAM_STR);
		$result->bindParam(':status', $status, PDO::PARAM_INT);
		return $result->execute();
	}

	public static function getStatusText($status)
	{
		switch ($status){
			case '1':
				return 'Новый заказ';
				break;
			case '2':
				return 'В обработке';
				break;
			case '3':
				return 'Доставляется';
				break;
			case '4':
				return 'Закрыт';
				break;
		}
	}

	public static function save($name, $phone, $comment, $user_id, $products)
	{
	    var_dump($products);
		$products = json_encode($products);

		$db = DB::getConnection();

		$sql = "INSERT INTO product_order (user_name, user_phone, user_comment, user_id, products) 
				VALUES (:name, :phone, :comment, :user_id, :products)";

		$result = $db->prepare($sql);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
		$result->bindParam(':phone', $phone, PDO::PARAM_STR);
		$result->bindParam(':comment', $comment, PDO::PARAM_STR);
		$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$result->bindParam(':products', $products, PDO::PARAM_STR);

		return $result->execute();
	}

	public static function delete($id)
	{
		$db = DB::getConnection();

		$sql = "DELETE FROM orders WHERE id = :id";

		$result = $db->prepare($sql);
		$result->bindParam(":id", $id, PDO::PARAM_INT);

		return $result->execute();
	}
}