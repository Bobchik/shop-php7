<?php
/**
 * Created by PhpStorm.
 * User: Bobka
 * Date: 02.12.2017
 * Time: 0:19
 */

class AdminController extends AdminBase {

	public function actionIndex()
	{
		self::checkAdmin();

		require_once(ROOT . '/views/admin/index.php');
		return true;
	}
}