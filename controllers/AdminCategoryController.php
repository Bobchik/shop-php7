<?php
/**
 * Created by PhpStorm.
 * User: Bobka
 * Date: 03.12.2017
 * Time: 22:18
 */

class AdminCategoryController extends AdminBase {

	public function actionIndex()
	{
		self::checkAdmin();

		$categoriesList = Category::getCategoryList();

		require_once (ROOT . "/views/admin_category/index.php");
		return true;
	}

	public function actionCreate()
	{
		self::checkAdmin();

			if (@($_POST['submit'])){
				$name = $_POST['name'];
				$sort_order = $_POST['sort_order'];
				$status = $_POST['status'];

				$errors = false;

				if (!@($_POST['name'])){
					$errors[] = "Заполните поля";
				}

				if ($errors == false){
					Category::createCategory($name, $sort_order, $status);

					header("Location: /admin/category");
				}
			}
		require_once (ROOT . "/views/admin_category/create.php");
		return true;
	}

	public function actionUpdate($id)
	{
		self::checkAdmin();

		$category = Category::getCategoryById($id);

			if (@($_POST['submit'])) {
				$name       = $_POST['name'];
				$sort_order = $_POST['sort_order'];
				$status     = $_POST['status'];

				Category::updateCategoryById( $id, $name, $sort_order, $status );

				header( "Location: /admin/category" );
			}
		require_once (ROOT . "/views/admin_category/update.php");
		return true;
	}

	public function actionDelete($id)
	{
		self::checkAdmin();

			if (@($_POST['submit'])){

				Category::deleteCategoryById($id);

				header( "Location: /admin/category" );
			}
		require_once (ROOT . "/views/admin_category/delete.php");
		return true;
	}
}