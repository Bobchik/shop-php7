<?php
/**
 * Created by PhpStorm.
 * User: Bobka
 * Date: 03.12.2017
 * Time: 22:19
 */

class AdminOrderController extends AdminBase {

	public function actionIndex()
	{

		self::checkAdmin();

		$ordersList = Order::getOrders();

		require_once (ROOT . "/views/admin_order/index.php");
		return true;

	}

	public function actionView($id)
	{
		self::checkAdmin();

		$order = Order::getOrderById($id);

		$productsQuantity = json_decode($order['products'], true);
		$productsIds = array_keys($productsQuantity);

		$products = Product::getProductsByIds($productsIds);

		// Подключаем вид
		require_once(ROOT . '/views/admin_order/view.php');
		return true;

	}

	public function actionUpdate($id)
	{
		self::checkAdmin();

		$order = Order::getOrderById($id);

		$productsQuantity = json_decode($order['products'], true);
		$productsIds = array_keys($productsQuantity);

		$products = Product::getProductsByIds($productsIds);

		if (@($_POST['submit'])){
			$userName = $_POST['userName'];
			$userPhone = $_POST['userPhone'];
			$userComment = $_POST['userComment'];
			$date = $_POST['date'];
			$status = $_POST['status'];

			Order::updateOrderById($id, $userName, $userPhone, $userComment, $date, $status);

			header("Location: /admin/order/view/$id");
		}

		require_once (ROOT . "/views/admin_order/update.php");
		return true;
	}

	public function actionDelete($id)
	{
		self::checkAdmin();

		if (@($_POST['submit'])) {

			Order::delete( $id );

			header("Location: /admin/order/");
		}
		require_once (ROOT . "/views/admin_order/delete.php");
		return true;

	}
}
