<?php
/**
 * Created by PhpStorm.
 * User: Bobka
 * Date: 17.11.2017
 * Time: 9:46
 */

class SiteController {

	public function actionIndex()
	{
		$categories = Category::getCategoryList();

		$latestProducts = Product::getLatestProducts();

		$recommended_prod = Product::getRecommendedProducts();

		require_once (ROOT.'/views/site/index.php');

		return true;
	}

	public function actionSale()
    {
        $categories = Category::getCategoryList();

        $products = Product::getSalesProducts();

        require_once (ROOT.'/views/site/sale.php');

        return true;
    }

	public function actionContacts()
	{
		require_once (ROOT.'/views/site/contact.php');

		return true;
	}

	public function actionAbout()
	{
		require_once (ROOT.'/views/site/about.php');

		return true;
	}

}