<?php
/**
 * Created by PhpStorm.
 * User: Bobka
 * Date: 03.12.2017
 * Time: 14:58
 */

class AdminProductController extends AdminBase {

	public function actionIndex()
	{
		self::checkAdmin();

		$productsList = Product::getProductsList();

		require_once (ROOT . '/views/admin_product/index.php');
		return true;
	}

	public function actionCreate()
	{
		self::checkAdmin();

		$categoriesList = Category::getCategoryList();

		if (@($_POST['submit'])){
			$options['name'] = trim($_POST['name']);
			$options['category_id'] = $_POST['category_id'];
			$options['code'] = $_POST['code'];
			$options['price'] = $_POST['price'];
            $options['image'] = $_FILES['image']['name'];
            $options['availability'] = $_POST['availability'];
			$options['brand'] = $_POST['brand'];
			$options['description'] = trim($_POST['description']);
			$options['short_desc'] = trim($_POST['short_desc']);
			$options['is_new'] = $_POST['is_new'];
			$options['is_recommended'] = $_POST['is_recommended'];
			$options['status'] = $_POST['status'];

			$errors = false;

			if ($errors == false){
			    $id = Product::createProduct($options);


				if ($id){
					if (is_uploaded_file($_FILES['image']['tmp_name'])){
						move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/store/images/products/{$id}.jpg");
					}
				}
				header("Location: /admin/product");
			}
		}

		require_once (ROOT . '/views/admin_product/create.php');
		return true;
	}

	public function actionUpdate($id)
	{
		self::checkAdmin();
		$categoriesList = Category::getCategoryList();

		$product = Product::getProductById($id);

		if (@($_POST['submit'])){
			$options['name'] = trim($_POST['name']);
			$options['category_id'] = $_POST['category_id'];
			$options['code'] = $_POST['code'];
			$options['price'] = $_POST['price'];
			$options['image'] = $_FILES['image']['name'];
			$options['availability'] = $_POST['availability'];
			$options['brand'] = $_POST['brand'];
			$options['description'] = trim($_POST['description']);
			$options['short_desc'] = trim($_POST['short_desc']);
			$options['is_new'] = $_POST['is_new'];
			$options['is_recommended'] = $_POST['is_recommended'];
			$options['status'] = $_POST['status'];

			if (Product::updateProductById($id, $options)){
				if (is_uploaded_file($_FILES['image']['tmp_name'])){
					move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/store/images/products/{$id}.jpg");
				}
			}
			header("Location: /admin/product");
		}

		require_once (ROOT . "/views/admin_product/update.php");
		return true;

	}

	public function actionDelete($id)
	{
		self::checkAdmin();

		if (@($_POST['submit'])){

		Product::deleteProductById($id);

			header("Location: /admin/product");
		}

		require_once (ROOT . '/views/admin_product/delete.php');
		return true;
	}

}