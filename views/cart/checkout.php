<?php include ROOT . '/views/layouts/header.php'; ?>
<?php
require_once(ROOT . '/views/layouts/NovaPostApi.php');
?>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Каталог</h2>
						<div class="panel-group category-products">
							<?php foreach ($categories as $categoryItem): ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a href="/category/<?php echo $categoryItem['id']; ?>">
												<?php echo $categoryItem['name']; ?>
											</a>
										</h4>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>

				<div class="col-sm-9 padding-right">
					<div class="features_items">
						<h2 class="title text-center">Корзина</h2>


						<?php if ($result): ?>

							<div>Заказ оформлен. Мы Вам перезвоним.</div>

						<?php else: ?>

							<div class="col-md-4">Выбрано товаров: <?php echo $totalQuantity; ?>, на сумму: <?php echo $totalPrice; ?>$</div>
                        <br>

<!--							<div class="col-sm-4">-->
								<?php if (isset($errors) && is_array($errors)): ?>
									<ul>
										<?php foreach ($errors as $error): ?>
											<li> - <?php echo $error; ?></li>
										<?php endforeach; ?>
									</ul>
								<?php endif; ?>

								<div class="col-md-8">Для оформления заказа заполните форму. Наш менеджер свяжется с Вами.</div>
                        <hr>

                                <div class="col-md-6">
                                    <div class="login-form">
                                        <form action="#" method="post">

                                            <p>Ваше имя</p>
                                            <input type="text" name="userName" class="form-control" placeholder="" value="<?php echo $userName; ?>"/>

                                            <p>Номер телефона</p>
                                            <input type="text" name="userPhone" class="form-control" placeholder="" value="<?php echo $userPhone; ?>"/>

                                            <p>Комментарий к заказу</p>
                                            <input type="text" name="userComment" class="form-control" placeholder="Сообщение" value="<?php echo $userComment; ?>"/>

                                            <p>Город</p>
                                            <select class="form-control" id="cities" name="city">
                                               <?php
                                                foreach ($cities['data'] as $city) {
                                                    echo '<option value="'.$city['Ref'].'">'.$city['DescriptionRu'].'</option>';
                                                }?>
                                            </select>

                                            <p>Склад Новой почты</p>
                                            <select class="form-control" id="warehouses">
                                                <?php
                                                foreach ($wh['data'] as $warehouse) {
                                                    echo '<option value="'.$warehouse['Ref'].'">'.$warehouse['DescriptionRu'].'</option>';
                                                }?>
                                            </select>
                                            <br/>
                                            <br/>
                                            <input type="submit" name="submit" class="btn btn-success" value="Оформить" />
                                        </form>
                                    </div>
                                </div>
							</div>

						<?php endif; ?>

					</div>

				</div>
			</div>
<!--		</div>-->
	</section>


<?php include ROOT . '/views/layouts/footer.php'; ?>