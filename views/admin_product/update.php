<?php include ROOT . '/views/layouts/header_admin.php'; ?>

	<section>
		<div class="container">
			<div class="row">

				<br/>

				<div class="breadcrumbs">
					<ol class="breadcrumb">
						<li><a href="/admin">Админпанель</a></li>
						<li><a href="/admin/product">Управление товарами</a></li>
						<li class="active">Редактировать товар</li>
					</ol>
				</div>


				<h4>Редактировать товар #<?php echo $id; ?></h4>

				<br/>

				<div class="col-lg-12">
					<div class="login-form">
						<form action="#" method="post" enctype="multipart/form-data">

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="name">Название товара</label>
                                <div class="col-lg-8">
							        <input class="form-control" type="text" name="name" placeholder="" value="<?php echo $product['name']; ?>">
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="code">Артикул</label>
                                <div class="col-lg-8">
							        <input class="form-control" type="text" name="code" placeholder="" value="<?php echo $product['code']; ?>">
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="price">Стоимость, $</label>
                                <div class="col-lg-8">
							        <input class="form-control" type="text" name="price" placeholder="" value="<?php echo $product['price']; ?>">
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="category_id">Категория</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="category_id">
                                        <?php if (is_array($categoriesList)): ?>
                                            <?php foreach ($categoriesList as $category): ?>
                                                <option value="<?php echo $category['id']; ?>"
                                                    <?php if ($product['category_id'] == $category['id']) echo ' selected="selected"'; ?>>
                                                    <?php echo $category['name']; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="brand">Производитель</label>
                                <div class="col-lg-8">
							        <input class="form-control" type="text" name="brand" placeholder="" value="<?php echo $product['brand']; ?>">
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="image">Изображение товара</label>
                                <div class="col-lg-8">
                                    <img src="<?php echo Product::getImage($product['id']); ?>" width="200" alt="" />
                                    <input class="form-control" type="file" name="image" placeholder="" value="<?php echo $product['image']; ?>">
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="description">Детальное описание</label>
                                <div class="col-lg-8">
							        <textarea class="form-control" name="description"><?php echo $product['description']; ?></textarea>
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="short_desc">Краткое описание</label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" name="short_desc"><?php echo $product['short_desc']; ?></textarea>
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="availability">Наличие на складе</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="availability">
                                        <option value="1" <?php if ($product['availability'] == 1) echo ' selected="selected"'; ?>>Да</option>
                                        <option value="0" <?php if ($product['availability'] == 0) echo ' selected="selected"'; ?>>Нет</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="is_new">Новинка</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="is_new">
                                        <option value="1" <?php if ($product['is_new'] == 1) echo ' selected="selected"'; ?>>Да</option>
                                        <option value="0" <?php if ($product['is_new'] == 0) echo ' selected="selected"'; ?>>Нет</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3 " for="is_recommended">Рекомендуемые</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="is_recommended">
                                        <option value="1" <?php if ($product['is_recommended'] == 1) echo ' selected="selected"'; ?>>Да</option>
                                        <option value="0" <?php if ($product['is_recommended'] == 0) echo ' selected="selected"'; ?>>Нет</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="status">Статус</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="status">
                                        <option value="1" <?php if ($product['status'] == 1) echo ' selected="selected"'; ?>>Отображается</option>
                                        <option value="0" <?php if ($product['status'] == 0) echo ' selected="selected"'; ?>>Скрыт</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-2 col-lg-offset-5">
                                <input type="submit" name="submit" class="btn btn-" value="Сохранить">
                            </div>
						</form>
					</div>
				</div>

			</div>
		</div>
	</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>