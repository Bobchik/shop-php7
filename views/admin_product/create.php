<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
	<div class="container">
		<div class="row">

			<br/>

			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="/admin">Админпанель</a></li>
					<li><a href="/admin/product">Управление товарами</a></li>
					<li class="active">Редактировать товар</li>
				</ol>
			</div>


			<h4 class="text-center">Добавить новый товар</h4>

			<br/>

			<?php if (isset($errors) && is_array($errors)): ?>
				<ul>
					<?php foreach ($errors as $error): ?>
						<li> - <?php echo $error; ?></li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>

				<div class="login-form">
					<form action="#" method="post" class="form-horizontal" enctype="multipart/form-data">

                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="name">Название товара</label>
                            <div class="col-lg-8">
                                <input type="text" name="name" placeholder="" class="form-control" value="">
                            </div>
                        </div>

                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="code">Артикул</label>
                            <div class="col-lg-8">
                                <input class="form-control" type="text" name="code" placeholder="" value="">
                            </div>
                        </div>

                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="price">Стоимость, $</label>
                            <div class="col-lg-8">
						        <input class="form-control" type="text" name="price" placeholder="" value="">
                            </div>
                        </div>

                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="brand">Производитель</label>
                            <div class="col-lg-8">
						        <input class="form-control" type="text" name="brand" placeholder="" value="">
                            </div>
                        </div>

                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="category_id">Категория</label>
                            <div class="col-lg-8">
                                <select class="form-control" name="category_id">
									<?php if (is_array($categoriesList)): ?>
										<?php foreach ($categoriesList as $category): ?>
                                            <option value="<?php echo $category['id']; ?>">
												<?php echo $category['name']; ?>
                                            </option>
										<?php endforeach; ?>
									<?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="image">Изображение товара</label>
                            <div class="col-lg-8">
						        <input type="file" class="form-control" name="image" placeholder="" value="no-image" />
                            </div>
                        </div>


                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="description">Детальное описание</label>
                            <div class="col-lg-8">
						        <textarea class="form-control" name="description"></textarea>
                            </div>
                        </div>

                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="short_desc">Краткое описание</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" name="short_desc"></textarea>
                            </div>
                        </div>

                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3 " for="is_recommended">Рекомендуемые</label>
                            <div class="col-lg-8">
                                <select class="form-control" name="is_recommended">
                                    <option value="1" selected="selected">Да</option>
                                    <option value="0">Нет</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="availability">Наличие на складе</label>
                            <div class="col-lg-8">
                                <select class="form-control" name="availability">
                                    <option value="1" selected="selected">Да</option>
                                    <option value="0">Нет</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="is_new">Новинка</label>
                            <div class="col-lg-8">
                                <select class="form-control" name="is_new">
                                    <option value="1" selected="selected">Да</option>
                                    <option value="0">Нет</option>
                                </select>
                            </div>
                        </div>



                        <div class="col-lg-6 form-group">
                            <label class="col-lg-3" for="status">Статус</label>
                            <div class="col-lg-8">
                                <select class="form-control" name="status">
                                    <option value="1" selected="selected">Отображается</option>
                                    <option value="0">Скрыт</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-2 col-lg-offset-5">
						    <input type="submit" name="submit" class="btn btn-" value="Сохранить">
                        </div>
					</form>
				</div>
			</div>

		</div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>
