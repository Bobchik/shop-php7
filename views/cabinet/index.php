<?php include ROOT . '/views/layouts/header.php'; ?>

	<section>
		<div class="container">
			<div class="row">
                <div class="col-md-8 col-md-offset-3">
				    <h1>Кабинет пользователя <?php echo $user['name'];?></h1>
                </div>
                <div class="col-md-3">
                    <ul class="list-group">
                        <a class="list-group-item" href="/cabinet/edit">Редактировать данные</a>
                        <a class="list-group-item" href="/cabinet/history">Список покупок</a>
                    </ul>
                </div>
			</div>
		</div>
	</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>