<?php include ROOT . '/views/layouts/header.php'; ?>

	<section>
		<div class="container">
			<div class="row">

<!--				<div class="col-sm-4 col-sm-offset-4 padding-right">-->

					<?php if ($result): ?>
						<p>Данные отредактированы!</p>
					<?php else: ?>
						<?php if (isset($errors) && is_array($errors)): ?>
							<ul>
								<?php foreach ($errors as $error): ?>
									<li> - <?php echo $error; ?></li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>

						<div class="signup-form">
							<h2 class="text-center">Редактирование данных</h2>
							<form action="#" method="post" class="form-inline">

                                <div class="form-group">
                                <label for="name">Имя:</label>
								<input class="form-control" type="text" name="name" placeholder="Имя" value="<?php echo $name; ?>"/>
                                </div>

                                <div class="form-group">
                                <label for="password">Пароль:</label>
								<input class="form-control" type="password" name="password" placeholder="Пароль" value="<?php echo $password; ?>"/>
                                </div>
                                <br/><div class="form-group">
								<input type="submit" name="submit" class="btn btn-success" value="Сохранить" />
                                </div>
							</form>
						</div>

					<?php endif; ?>
					<br/>
					<br/>
				</div>
			</div>
<!--		</div>-->
	</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>