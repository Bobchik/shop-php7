<?php include ROOT . '/views/layouts/header_admin.php'; ?>

	<section>
		<div class="container">
			<div class="row">

				<br/>

				<div class="breadcrumbs">
					<ol class="breadcrumb">
						<li><a href="/admin">Админпанель</a></li>
						<li><a href="/admin/order">Управление заказами</a></li>
						<li class="active">Просмотр заказа</li>
					</ol>
				</div>


				<h4>Просмотр заказа #<?php echo $order['id']; ?></h4>
				<br/>



                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h5 class="text-center">Информация о заказе</h5>
                            <br>
                            <div class="login-form">
                                <form action="#" method="post" >
                                    <div class="col-lg-6 form-group">
                                        <label class="col-lg-3" for="order_id">Номер заказа</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" type="text" name="order_id" value="<?php echo $order['id']; ?>">
                                        </div>
                                    </div>

                                    <div class="col-lg-6 form-group">
                                        <label class="col-lg-3" for="userName">Имя клиента</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" type="text" name="userName" value="<?php echo $order['user_name']; ?>">
                                        </div>
                                    </div>

                                    <div class="col-lg-6 form-group">
                                        <label class="col-lg-3" for="userPhone">Телефон</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" type="text" name="userPhone" value="<?php echo $order['user_phone']; ?>">
                                        </div>
                                    </div>

                                    <div class="col-lg-6 form-group">
                                        <label class="col-lg-3" for="userComment">Комментарий к заказу</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" type="text" name="userComment" value="<?php echo $order['user_comment']; ?>">
                                        </div>
                                    </div>

				                    <?php if ($order['user_id'] != 0): ?>
                                    <div class="col-lg-6 form-group">
                                        <label class="col-lg-3" for="user_id">ID клиента</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" type="text" name="user_id" value="<?php echo $order['user_id']; ?>">
                                        </div>
                                    </div>
				                    <?php endif; ?>

                                    <div class="col-lg-6 form-group">
                                        <label class="col-lg-3" for="status">Статус</label>
                                        <div class="col-lg-8">
                                            <select class="form-control" name="status">
                                                <option value="1" >Новый заказ</option>
                                                <option value="2" >В обработке</option>
                                                <option value="3" >Доставляется</option>
                                                <option value="4" >Закрыт</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 form-group">
                                        <label class="col-lg-3" for="date">Дата заказа</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" type="text" name="date" value="<?php echo $order['date']; ?>">
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-lg-offset-5 ">
                                        <input type="submit" name="submit" class="btn btn-success" value="Сохранить">
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>


				<h5>Товары в заказе</h5>

				<table class="table-admin-medium table-bordered table-striped table ">
					<tr>
						<th>ID товара</th>
						<th>Артикул товара</th>
						<th>Название</th>
						<th>Цена</th>
						<th>Количество</th>
					</tr>
					<?php foreach ($products as $product): ?>
						<tr>
							<td><?php echo $product['id']; ?></td>
							<td><?php echo $product['code']; ?></td>
							<td><?php echo $product['name']; ?></td>
							<td>$<?php echo $product['price']; ?></td>
							<td><?php echo $productsQuantity[$product['id']]; ?></td>
						</tr>
					<?php endforeach; ?>
				</table>

				<a href="/admin/order/" class="btn btn-default back"><i class="fa fa-arrow-left"></i> Назад</a>
			</div>


	</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>