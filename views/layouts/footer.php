<br/>
<br/>
<footer id="footer"><!--Footer-->
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<p class="pull-left">Техно Ярмарок © 2017</p>
				<p class="pull-right">Связь по почте:<a href="mailto:techno-yarmarka@gmail.com"> techno-yarmarka@gmail.com</a></p>
			</div>
		</div>
	</div>
</footer><!--/Footer-->



<script src="/template/js/jquery.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/price-range.js"></script>
<script src="/template/js/jquery.prettyPhoto.js"></script>
<script src="/template/js/main.js"></script>
<script src="/template/js/jquery.cycle2.min.js"></script>
<script src="/template/js/jquery.cycle2.carousel.min.js"></script>
<script src="/template/js/map.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADsSs3c_HJ1FZ2TPvdeyb3Dm0cf9iqP4U&callback=initMap" type="text/javascript">
</script>
<script>
    $(document).ready(function () {
        $('.add-to-cart').click(function () {
            var id = $(this).attr("data-id");
            $.post("/cart/addAjax/"+id, {}, function (data) {
                $("#cart-count").html(data);
            });
            return false;
        });
    });
</script>
<script>

    // load cities
//    $("#cities").load( "file.php" );

    // get warehouses
    $('#cities').change(function(){
        var wh = $(this).val();

        $.ajax({
            url: 'NovaPostApi.php',
            type: 'POST',
            data: {
                'warehouses': wh,
            },
            success: function (data) {
                alert('Good');
                $('#warehouses').html(data);
            },
            error: function (request, error) {
                $('#warehouses').html('<option>-</option>');
            }
        })
    });
</script>
</body>
</html>
