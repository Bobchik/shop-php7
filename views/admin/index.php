<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
	<div class="container">
		<div class="row">

			<br/>



            <br>

            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4>Добрый день, администратор!</h4>
                                <h3 class="panel-title">
<!--                                    <span class="glyphicon glyphicon-bookmark"></span>Панель управления:</h3>-->
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-4 col-md-4">
                                        <a href="/admin/product" class="btn btn-danger btn-lg" role="button"><span class="glyphicon glyphicon-list-alt"></span> <br/>Управление товарами</a>
                                    </div>
                                    <div class="col-xs-4 col-md-4">
                                        <a href="/admin/order" class="btn btn-warning btn-lg" role="button"><span class="glyphicon glyphicon-bookmark"></span> <br/>Управление заказами</a>
                                    </div>
<!--                                        <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-signal"></span> <br/>Reports</a>-->
<!--                                        <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-comment"></span> <br/>Comments</a>-->
<!--                                        <a href="#" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-user"></span> <br/>Users</a>-->
                                    <div class="col-xs-4 col-md-4">
                                        <a href="/admin/category" class="btn btn-info btn-lg" role="button"><span class="glyphicon glyphicon-file"></span> <br/>Управление категориями</a>
                                    </div>
<!--                                        <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-picture"></span> <br/>Photos</a>-->
<!--                                        <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-tag"></span> <br/>Tags</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

		</div>
	</div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

