<?php include ROOT . '/views/layouts/header_admin.php'; ?>

	<section>
		<div class="container">
			<div class="row">

				<br/>

				<div class="breadcrumbs">
					<ol class="breadcrumb">
						<li><a href="/admin">Админпанель</a></li>
						<li><a href="/admin/category">Управление категориями</a></li>
						<li class="active">Добавить категорию</li>
					</ol>
				</div>


				<h4>Добавить новую категорию</h4>

				<br/>

				<?php if (isset($errors) && is_array($errors)): ?>
					<ul>
						<?php foreach ($errors as $error): ?>
							<li> - <?php echo $error; ?></li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>

				<div class="col-lg-12">
					<div class="login-form">
						<form action="#" method="post">

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="name">Название</label>
                                <div class="col-lg-8">
							        <input class="form-control" type="text" name="name" placeholder="" value="">
                                </div>
                            </div>


                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="sort_order">Порядковый номер</label>
                                <div class="col-lg-8">
							        <input class="form-control" type="text" name="sort_order" placeholder="" value="">
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="status">Статус</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="status">
                                        <option value="1" selected="selected">Отображается</option>
                                        <option value="0">Скрыта</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-2 col-lg-offset-5 col-lg-pull-5">
                                <input type="submit" name="submit" class="btn btn-success" value="Сохранить">
                            </div>
						</form>
					</div>
				</div>


			</div>
		</div>
	</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>