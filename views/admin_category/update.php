<?php include ROOT . '/views/layouts/header_admin.php'; ?>

	<section>
		<div class="container">
			<div class="row">

				<br/>

				<div class="breadcrumbs">
					<ol class="breadcrumb">
						<li><a href="/admin">Админпанель</a></li>
						<li><a href="/admin/category">Управление категориями</a></li>
						<li class="active">Редактировать категорию</li>
					</ol>
				</div>


				<h4>Редактировать категорию "<?php echo $category['name']; ?>"</h4>

				<br/>

				<div class="col-lg-12">
					<div class="login-form">
						<form action="#" method="post">

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="name">Название</label>
                                <div class="col-lg-8">
							        <input class="form-control" type="text" name="name" placeholder="" value="<?php echo $category['name']; ?>">
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="sort_order">Порядковый номер</label>
                                <div class="col-lg-8">
							        <input class="form-control" type="text" name="sort_order" placeholder="" value="<?php echo $category['sort_order']; ?>">
                                </div>
                            </div>


                            <div class="col-lg-6 form-group">
                                <label class="col-lg-3" for="status">Статус</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="status">
                                        <option value="1" <?php if ($category['status'] == 1) echo ' selected="selected"'; ?>>Отображается</option>
                                        <option value="0" <?php if ($category['status'] == 0) echo ' selected="selected"'; ?>>Скрыта</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-2 col-lg-offset-5 col-lg-pull-5">
							    <input type="submit" name="submit" class="btn btn-success" value="Сохранить">
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>