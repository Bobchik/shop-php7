<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
           <h2 class="text-center">O нас</h2>
            <p><span class="text-primary">Техно ярмарок</span> – с нами просто, удобно и надежно делать покупки в интернете.</p>
            <p>Интернет магазин TechIt.ua – одно из подразделений компании Brain Computers. Собственные региональные склады, сервисные центры и пункты выдачи в 23 областях Украины - гарантия качественного и быстрого выполнения Ваших заказов.</p>
            <p>За 25 лет работы на рынке мы выстроили тесные партнерские отношения с официальными представителями всех мировых и украинских брендов компьютерной и бытовой техники.</p>
            <p>Также, мы являемся официальным представителем и импортером торговых марок Exceleram и Apacer в Украине.</p>
            <p>Наша собственная торговая марка Tech – это лучшее сочетание цена-качество при выборе телевизоров, сетевого оборудования, компьютерной техники, разнообразных гаджетов и аксессуаров.</p>

            <ul class="seo-list text-center">
                <li class="seo-item">
                    <div class="seo-icon">
                        <img src="https://static.itbox.ua/static/src/img/content/about/new-truck.png" alt="img">
                    </div>
                    <h4 class="seo-head">Быстро</h4>
                        <p>наши склады в вашем городе</p>
                </li>
                <li class="seo-item">
                    <div class="seo-icon">
                        <img src="https://static.itbox.ua/static/src/img/content/about/hands.png" alt="img">
                    </div>
                    <h4 class="seo-head">Надежно</h4>
                        <p>проверка товара до оплаты</p>
                        <p>возврат товара без проблем</p>
                        <p>сервисные центры в вашем городе</p>
                </li>
                <li class="seo-item">
                    <div class="seo-icon">
                        <img src="https://static.itbox.ua/static/src/img/content/about/flower.png" alt="img">
                    </div>
                    <h4 class="seo-head">Выгодно</h4>
                        <p>бесплатная доставка в регионы</p>
                        <p>программы лояльности</p>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>