<?php include ROOT . '/views/layouts/header.php'; ?>

	<div class="container contacts">

		<div class="row row-offcanvas row-offcanvas-right">

			<div class="col-xs-12 col-sm-12">
				<div class="text-center">
					<h2>Техно ярмарок</h2>
					<p>Мы находимся в центре города Балта, по улице Любомирской 221</p>
					<p>График работы: с 9:00 до 18:00 без выходных</p>
					<p>За дополнительной информацией можете обращаться по телефонам:</p>
				</div>
					<div class="col-xs-6 col-lg-4">
						<h2>Информация о заказах:</h2>
						<p> +38 (093) 745 21 21</p>
						<p> +38 (095) 745 21 21</p>
						<p> +38 (097) 745 21 21</p>
					</div><!--/.col-xs-6.col-lg-4-->
					<div class="col-xs-6 col-lg-4">
						<h2>Консультация от продавца:</h2>
						<p> +38 (093) 745 21 20</p>
						<p> +38 (095) 745 21 20</p>
						<p> +38 (097) 745 21 20</p>
					</div><!--/.col-xs-6.col-lg-4-->
					<div class="col-xs-6 col-lg-4">
						<h2>Консультация по рассрочке:  </h2>
						<p> +38 (093) 745 21 20</p>
						<p> +38 (095) 745 21 20</p>
						<p> +38 (097) 745 21 20</p>
				</div><!--/.col-xs-6.col-lg-4-->

                <div class="sidebar-offcanvas" id="sidebar">
                    <div id="map"></div>
                </div>
			</div><!--/.col-xs-12.col-sm-9-->
		</div><!--/row-->
	</div><!--/.container-->

<?php include ROOT . '/views/layouts/footer.php'; ?>