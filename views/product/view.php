<?php include (ROOT."/views/layouts/header.php");?>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					<h2>Каталог</h2>
					<div class="panel-group category-products" id="accordian"><!--category-productsr-->
					<?php foreach ( $categories as $category ) : ?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a href="/category/<?php echo $category['id']; ?>"><?php echo $category['name']; ?></a></h4>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
        </div>

			<div class="col-sm-9 padding-right">
				<div class="product-details"><!--product-details-->
					<div class="row">
						<div class="col-sm-5">
							<div class="view-product">
								<img class="text-center" src="<?php echo Product::getImage($product['id']);?>" alt="" />
							</div>
						</div>
						<div class="col-sm-7 col-sm-offset-0">
							<div class="product-information"><!--/product-information-->
                                <?php if ($product['is_new']): ?>
								<img src="/template/images/product-details/new.jpg" class="newarrival" alt="" />
                                <?php endif; ?>
								<h2><?php echo $product['name']; ?></h2>
								<p><?php echo $product['code']; ?></p>
								<div>
                                    <div>&#x24;<?php echo $product['price']; ?></div>
<!--                                    <br>-->
<!--                                    <label>Количество:</label>-->
<!--                                    <input class="count" type="text" value=""/>-->
                                </div>
								<p><b>Наличие:</b> На складе</p>
								<p><b>Состояние:</b> Новое</p>
								<p><b>Производитель:</b><?php echo $product['brand']; ?></p>
                                <a href="#" data-id="<?php echo $product['id'];?>" class="btn add-to-cart">
                                    <i class="fa fa-shopping-cart"></i>
                                    В корзину
                                </a>
							</div><!--/product-information-->
						</div>
					</div>
                    <hr>
                    <div class="row">
						<div class="col-sm-12">
							<h5>Описание товара</h5>
							<p><?php echo $product['description']; ?></p>
						</div>
					</div>
				</div><!--/product-details-->
			</div>
		</div>
	</div>
</section>

<?php include (ROOT."/views/layouts/footer.php");?>